#include <iostream>
#include <cmath>
using namespace std;
class figura
{
	public:
		float pole;
		float obwod;
	void read()
	{
		cout<<"Pole: "<<pole<<endl;
		cout<<"Obwod: :"<<obwod<<endl;
	}
};
class kolo:public figura
{
	public:
		float r;
	kolo(float x)
	{
		r=x;
		obwod=2*r*M_PI;
		pole=M_PI*r*r;
	}
};
class prostokat:public figura
{
	public:
	float a,b;
	prostokat(float x,float y)
	{
		a=x;
		b=y;	
		pole=a*x;
		obwod=a+a+b+b;
	}	
};
int main()
{
	kolo k(3);
	prostokat p(10,25);
	k.read();
	p.read();
	return 0;	
};
