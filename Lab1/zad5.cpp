#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
using namespace std;
class wectorn
{
	private:
		float N;
		float *tab;
 		fstream plik;

	public:
		wectorn(int n)
		{
			N=n;
			plik.open("tablicaN.txt",ios::out);
        	if (plik.good() == false)
       			 {
          			cout << "Plik nie dziala"<<endl;
        		}
        	else
       		{
            		cout << "Plik otwarty"<<endl;
            try
            {
				tab = new float[n];
				srand( time( NULL ) );
				for(int i=0;i<n;i++)
					{
						tab[i]=rand()%100;
						plik<<tab[i]<<endl;
					}
			}
			catch(...)
            {
                cout << "Nieudana alokacja pamieci";
            }
            plik.close();
			}
		};
		~wectorn()
		{
			delete tab;
		}
		void length()
		{
		cout<<"Wymiar wektora to: "<<N<<endl;
		};
		void at(int i)
		{
			cout<<"wartosc wektora w indeksie "<<i<<" to "<< tab[i]<<endl;
		};
};

int main()
{
	wectorn vector(5);
	vector.length();
	vector.at(3);
	vector.~wectorn();
	return 0;
}
